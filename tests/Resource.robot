*** Settings ***
Library         SeleniumLibrary

*** Variables ***
${BROWSER}    chrome
${URL_SEND4}        https://demo.sprint.troquefacil.com.br/
${TITLE_PAGE}       Send4  | Send4
${ACAO_LABEL}       xpath=//*[@class='select-default ']/select[@name='product-ecommerce-action-bd63d56a404f45547320ebcb3e86421e']
${MOTIVOS}          xpath=//*[@class='select-default ']/select[@name='product-ecommerce-reason-bd63d56a404f45547320ebcb3e86421e']
*** Keywords ***
#### Setup e Teardown
Abrir navegador
    Open Browser    about:blank   ${BROWSER}
    Maximize Browser Window

Fechar navegador
    # Close Browser

#### Ações
Acessar a página home do site
    Go To    ${URL_SEND4}
    Sleep    2
    Title Should Be    ${TITLE_PAGE}

Clicar em começar
    Click Element    id=introduction__start
    Sleep    2

Selecionar a opção loja virtual
    Click Element    xpath=//*[@alt='Loja Virtual']
    Sleep    2

Entrar com o pedido "${PEDIDO}" e o email "${EMAIL}"
    Click Element   id=order-number
    Input Text      id=order-number    ${PEDIDO}

    Click Element   id=order-confirmation
    Input Text      id=order-confirmation    ${EMAIL}

Clicar no botão buscar e continuar
    Click Element    id=order__continue
    Sleep    2

### Cenário 02
Selecionar item
    Click Element    xpath=//h4[@title='Camiseta Send4Lovers']
    Sleep    2

Selecionar a ação os valores para troca
    Select From List By Value    xpath=//*[@class='select-default ']/select[@name='product-ecommerce-qty-bd63d56a404f45547320ebcb3e86421e']    3
    Sleep    2

    Click Element                ${ACAO_LABEL}
    Select From List By Label    ${ACAO_LABEL}      Trocar
    Sleep    2

    Select From List By Value    ${MOTIVOS}    2096
    Sleep    2

Preencher o motivo "${COMENTARIO}"
    Click Element   xpath=//*[@class='textarea-default']/textarea[@name='product-ecommerce-comment-bd63d56a404f45547320ebcb3e86421e']
    Input Text      xpath=//*[@class='textarea-default']/textarea[@name='product-ecommerce-comment-bd63d56a404f45547320ebcb3e86421e']    ${COMENTARIO}

Clicar em continuar
    Click Element    id=products-ecommerce__continue
    Sleep    2
Selecionar forma de envio Correios e continuar
    Click Element    xpath=//*[@class='couriers-title']
    Click Element    id=shipping__continue

Conferir o nome do produto a ser trocado
    Element Text Should Be    xpath=//div[@class='product-item-details']/div[@data-title='Produto']     Camiseta Send4Lovers

Clicar no botão continuar
    Click Element    id=resume__continue
    Sleep    5

Selecionar uma nota de avaliação preencher comentário "${NOTA}" Enviar avaliação
    Click Element    xpath=//div[@class='list-container']/div/ul/li[@id='rating-star-8']
    Sleep    2

    Click Element    xpath=//div[@class='textarea-default']/textarea[@id='finish-comment']
    Input Text    xpath=//div[@class='textarea-default']/textarea[@id='finish-comment']    ${NOTA}

    Click Element    id=finish__send-rating
    Sleep    2
