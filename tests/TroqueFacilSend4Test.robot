*** Settings ***
Suite Setup          Abrir navegador
# Suite Teardown       Fechar navegador
Resource             resource.robot

*** Test Case ***
# Cenário 01: Acessar aplicação Send4
#     Acessar a página home do site
#     Clicar em começar
#     Selecionar a opção loja virtual
#     Entrar com o pedido "318" e o email "tester@send4.com.br"
#     Clicar no botão buscar e continuar

Cenário 02: Efetuar troca pedido
    Acessar a página home do site
    Clicar em começar
    Selecionar a opção loja virtual
    Entrar com o pedido "318" e o email "tester@send4.com.br"
    Clicar no botão buscar e continuar
    Selecionar item
    Selecionar a ação os valores para troca
    Preencher o motivo "Troca produto teste"
    Clicar em continuar
    Selecionar forma de envio Correios e continuar
    Conferir o nome do produto a ser trocado
    Clicar no botão continuar
    Selecionar uma nota de avaliação preencher comentário "Nota 8" Enviar avaliação

Cenário 02: Efetuar troca pedido
    Acessar a página home do site
    Clicar em começar
    Selecionar a opção loja virtual
    Entrar com o pedido "318" e o email "tester@send4.com.br"
    Clicar no botão buscar e continuar
    Selecionar item
    Selecionar a ação os devolver
    Preencher o motivo "Me arrependi"
    Preencher Como podemos resolver
    Clicar em continuar
    # Selecionar forma de envio Correios e continuar
    # Conferir o nome do produto a ser trocado
    # Clicar no botão continuar
    # Selecionar uma nota de avaliação preencher comentário "Nota 8" Enviar avaliação
